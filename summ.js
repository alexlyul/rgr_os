module.exports = matrixes => {
    const params = new Set();
    for (let i = 0; i < matrixes.length; i++) {
        params.add(matrixes[i][0].length); //W
        params.add(matrixes[i].length);    //H
    }
    if (params.size > 2) throw new Error('Matrices should be same width and height');
    const resp = [...matrixes[0]];
    for (let i = 1; i < matrixes.length; i++) {
        resp.forEach((row, rowIndex) => {
            row.forEach((num, numIndex) => {
                resp[rowIndex][numIndex] += matrixes[i][rowIndex][numIndex];
            });
        });
    }
    return resp;
};
