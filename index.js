const fs = require('fs');
const path = require('path');
let width = 0;
let num = 0;
//node index.js matrix -width 4 -num 2
if (process.argv.length > 2 && process.argv.length <= 7) {
    for (let i = 2; i < 7; i++) {
        console.log(i + ': ' + process.argv[i]);
        if (process.argv[i] === '-width') {
            width = parseInt(process.argv[i + 1]);
        } else if (process.argv[i] === '-num') {
            num = parseInt(process.argv[i + 1]);
        }
    }
    let file = process.argv[2];
    if (!path.isAbsolute(file)) {
        file = path.join(__dirname, file);
    }
    fs.exists(file, exist => {
        if (exist) {
            console.log('Found path:', file);
            fs.readFile(file, 'utf8', function (err, data) {
                let parsedData = [];
                if (err) throw err;
                console.log(`ARGUMENTS: matrix width: ${width}, number: ${num}`);

                data.split(";").forEach(matrix => {
                    if(matrix.length > 1) {
                        matrix = matrix.split('\n').filter(item => {return item !== ''});
                        parsedData.push([]);
                        matrix.forEach(row => {
                            const newMatrix = parsedData[parsedData.length - 1];
                            newMatrix.push([]);
                            if(row.length !== 0) {
                                row.split(' ').forEach((str) => {
                                    newMatrix[newMatrix.length - 1].push(parseInt(str));
                                });
                            }
                        });
                    }
                });
                console.log('Received data: ', "\n", parsedData);


                console.log("RESULT:\n", require('./summ')(parsedData) );
            });
        } else {
            throw new Error('File:' + file + ' does not found');
        }
    });


} else {
    throw new Error(`Error in arguments list. Example: "node index.js matrixfile -width 4 -num 2" \nreceived: ${process.argv.join(' ')}`);
}

